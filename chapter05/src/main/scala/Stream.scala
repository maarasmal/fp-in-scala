
package org.mlamarre.fpinscala.chapter05

import annotation.tailrec

sealed trait Stream[+A] {
  def headOption: Option[A] = this match {
    case Empty => None
    // NOTE: Explicit forcing of the h thunk using h()
    case Cons(h, t) => Some(h())
  }

  // NOTE: This was my initial implementation. Upon checking the answers, I saw
  // that this matched one of their implementations: the one that would blow the
  // stack for large streams. Oops. Below is a tail-recursive solution.
  def myToList: List[A] =
    this match {
      case Empty => List.empty
      case Cons(h, t) => h() :: t().myToList
    }

  def toList: List[A] = {
    @tailrec
    def loopR(s: Stream[A], acc: List[A]): List[A] =
      s match {
        case Empty => acc
        case Cons(h, t) => loopR(t(), h() :: acc)
      }
    loopR(this, List.empty[A]).reverse
  }

  // NOTE: My implementations of take/drop appear to work fine, but look like
  // they'd iterate once more than the solutions from the answer key. The 'if'
  // can be consolidated into the match/case via guards. Also, drop could be
  // tail recursive.
  def take(n: Int): Stream[A] = {
    if (n < 1)
      Stream.empty
    else {
      this match {
        case Empty => Stream.empty
        case Cons(h, t) => Stream.cons(h(), t().take(n - 1))
      }
    }
  }

  def drop(n: Int): Stream[A] = {
    if (n < 1)
      this
    else {
      this match {
        case Empty => Stream.empty
        case Cons(h, t) => t().drop(n - 1)
      }
    }
  }

  def takeWhile(p: A => Boolean): Stream[A] = {
    this match {
      case Cons(h, t) if p(h()) => Stream.cons(h(), t().takeWhile(p))
      case _ => Stream.empty
    }
  }

}

case object Empty extends Stream[Nothing]
// A non-empty stream consists of a head and a tail, which are both non-strict.
// Due to technical limitations, these are thunks that must be explicitly
// forced, rather than by-name parameters
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  // Creates an empty stream for a particular type A
  def empty[A]: Stream[A] = Empty

  // A variable-argument method for constructing a Stream from
  // multiple elements
  def apply[A](as: A*): Stream[A] = if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))
}
