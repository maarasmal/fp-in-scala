## Functional Programming in Scala - Solutions by M. Lamarre
This project contains my solutions for the exercises from the book Functional Programming in Scala, 1st Edition. I am also using it as a bit of a sandbox to learn a bit about things like SBT and markdown documentation.


