
package org.mlamarre.fpinscala.chapter05.test

import org.mlamarre.fpinscala.chapter05._
import org.scalatest.{ FunSuite, Matchers }

class Chapter05Tests extends FunSuite with Matchers {

  test("Exercise 5.1 - Basic stream operations and toList") {
    Stream.empty[Int].headOption should be(None)
    val s = Stream(1, 2, 3, 4, 5)
    s.headOption should be(Some(1))

    // toList
    Stream(4, 5, 6, 7).toList shouldBe List(4, 5, 6, 7)
    Stream.empty[String].toList shouldBe Nil
  }

  test("Exercise 5.2 - take() and drop() for Stream") {
    val s = Stream(1, 2, 3, 4, 5)
    s.take(2).toList shouldBe List(1, 2)

    Stream.empty[Int].take(2).toList shouldBe Nil
    s.take(-1).toList shouldBe Nil
    s.take(0).toList shouldBe Nil

    val m = Stream({ println("hi"); 1 }, 2, 3, 4, 5)
    m.take(2).toList shouldBe List(1, 2)
    m.take(3).toList shouldBe List(1, 2, 3)

    s.drop(0).toList shouldBe List(1, 2, 3, 4, 5)
    s.drop(-1).toList shouldBe List(1, 2, 3, 4, 5)
    s.drop(2).toList shouldBe List(3, 4, 5)
    s.drop(10).toList shouldBe List.empty
    Stream.empty[Int].drop(12).toList shouldBe List.empty
  }

  test("Exercise 5.3 - takeWhile on Stream") {
    val s = Stream(1, 2, 3, 4, 5, 6, 99, 100, 101)
    s.takeWhile(_ < 100).toList shouldBe List(1, 2, 3, 4, 5, 6, 99)
  }
}
