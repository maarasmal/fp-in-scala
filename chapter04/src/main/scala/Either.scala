
package org.mlamarre.fpinscala.chapter04

import scala.{ Option => _, Either => _, _ }

sealed trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B] =
    this match {
      case Left(e) => Left(e)
      case Right(a) => Right(f(a))
    }

  // When mapping over the right side, we must promote the left type parameter to
  // some supertype, to satisfy the +E variance annotation.
  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] =
    this match {
      case Left(e) => Left(e)
      case Right(a) => f(a)
    }

  def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B] =
    this match {
      case Right(a) => Right(a)
      case Left(_) => b
    }

  def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C) =
    this flatMap { a => b map { bb => f(a, bb) } }

  // I added this just to mirror our Option type, not for any exercise
  def getOrElse[B >: A](default: => B): B =
    this match {
      case Left(_) => default
      case Right(a) => a
    }

}

case class Left[+E](value: E) extends Either[E, Nothing]
case class Right[+A](value: A) extends Either[Nothing, A]

object Either {

  def Try[A](a: => A): Either[Exception, A] =
    try Right(a)
    catch { case e: Exception => Left(e) }

  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] =
    traverse(es)(identity)

  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] =
    as.foldRight[Either[E, List[B]]](Right(List()))((a, el) => f(a).map2(el)(_ :: _))
  // This was my initial implementation, but it doesn't correctly result in holding the first error
  // in 'as' because we end up not calling f() after we encounter the first Left, but since we are
  // folding from the RIGHT, we get the LAST error in the original list.
  // as.foldRight[Either[E, List[B]]](Right(List()))((a, el) => el flatMap (l => f(a) map (b => b :: l)))

}
