
package org.mlamarre.fpinscala.chapter03

import scala.annotation.tailrec

object Chapter03 {

  sealed trait List[+A]

  case object Nil extends List[Nothing]

  case class Cons[+A](head: A, tail: List[A]) extends List[A]

  object List {
    def sum(ints: List[Int]): Int = ints match {
      case Nil => 0
      case Cons(x, xs) => x + sum(xs)
    }

    def product(ds: List[Double]): Double = ds match {
      case Nil => 1.0
      case Cons(0.0, _) => 0.0
      case Cons(x, xs) => x * product(xs)
    }

    def apply[A](as: A*): List[A] =
      if (as.isEmpty) Nil
      else Cons(as.head, apply(as.tail: _*))

    // For exercise 3.2
    def tail[A](xs: List[A]): List[A] = xs match {
      // My original Nil case just returned Nil, but the answers pointed out that if you take the
      // tail of an empty list in practice, this is usually a coding error. In fact, the standard
      // List class in Scala does throw this exception for "List().tail" as well, so I updated
      // my answer here.
      case Nil => throw new UnsupportedOperationException("Can't take the tail of an empty list")
      case Cons(_, t) => t
    }

    // For exercise 3.3
    def setHead[A](x: A, xs: List[A]): List[A] = xs match {
      // The answers also threw an exception for this Nil case. I'm not sure I agree with that.
      // That said, the standard Scala List throws an UnsupportedOperationException if you attempt
      // to call List().updated(0, 4), so I went ahead and changed this to behave similarly.
      case Nil => throw new UnsupportedOperationException("Can't set the head of an empty list")
      case Cons(_, t) => Cons(x, t)
    }

    // For exercise 3.4
    def drop[A](l: List[A], n: Int): List[A] =
      if (n <= 0) l
      else l match {
        case Nil => Nil
        case Cons(_, t) => drop(t, n - 1)
      }

    // For exercise 3.5
    def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
      // I won't bother changing my solution to match, but the answers had a slicker implementation
      // that first matched Cons(h,t) and used a guard condition to apply f to the head. The second
      // case just returned the original list. That's a little cleaner than this, but functionally
      // equivalent.
      case Nil => Nil
      case Cons(h, t) => if (f(h)) dropWhile(t, f) else l
    }

    def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
      case Nil => a2
      case Cons(h, t) => Cons(h, append(t, a2))
    }

    // For exercise 3.6
    def init[A](l: List[A]): List[A] = l match {
      case Nil => throw new UnsupportedOperationException("Cannot init an empty list")
      case Cons(_, Nil) => Nil
      case Cons(h, t) => Cons(h, init(t))
    }

    def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
      case Nil => z
      case Cons(x, xs) => f(x, foldRight(xs, z)(f))
    }

    def sum2(ns: List[Int]) = foldRight(ns, 0)((x, y) => x + y)

    def product2(ns: List[Double]) = foldRight(ns, 1.0)(_ * _)

    // For exercise 3.9
    def length[A](as: List[A]): Int = foldRight(as, 0)((_, acc) => acc + 1)

    // For exercise 3.10
    @tailrec
    def foldLeft[A, B](as: List[A], z: B)(f: (B, A) => B): B = as match {
      case Nil => z
      case Cons(h, t) => foldLeft(t, f(z, h))(f)
    }

    // For exercise 3.11
    def sum3(l: List[Int]): Int = foldLeft(l, 0)((acc, x) => acc + x)

    // For exercise 3.11
    def product3(l: List[Double]): Double = foldLeft(l, 1.0)((acc, x) => acc * x)

    // For exercise 3.11
    def length2[A](l: List[A]): Int = foldLeft(l, 0)((acc, _) => acc + 1)

    // For exercise 3.12
    def reverse[A](l: List[A]): List[A] = foldLeft(l, Nil: List[A])((x, y) => Cons(y, x))

    // For exercise 3.13
    def foldLeftViaFoldRight[A, B](l: List[A], z: B)(f: (B, A) => B): B =
      foldRight(reverse(l), z)((x, y) => f(y, x))

    // For exercise 3.13
    // This gives us a stack-safe version of foldRight. The previous implementation above could
    // blow the stack for large lists, as it wasn't tail-recursive. foldLeft and reverse *are*
    // tail-recursive, so this implementation of foldRight is stack-safe.
    def foldRightViaFoldLeft[A, B](as: List[A], z: B)(f: (A, B) => B): B =
      foldLeft(reverse(as), z)((x, y) => f(y, x))

    // For exercise 3.14
    // Note that the value of 'f' passed to foldRight could have just been "Cons(_,_)"
    def append2[A](a1: List[A], a2: List[A]): List[A] = foldRightViaFoldLeft(a1, a2)((x, y) => Cons(x, y))

    // For exercise 3.15
    // Note that you must use either List[A]() or Nil:List[A] for the 'z' argument to avoid a compile error.
    // If you do not, the compiler will assume Nothing for the type A in that Nil.
    def concat[A](l: List[List[A]]): List[A] = foldRightViaFoldLeft(l, List[A]())(append2)

    // For exercise 3.16
    def plusOne(l: List[Int]): List[Int] =
      foldRightViaFoldLeft(l, Nil: List[Int])((x, acc) => Cons(x + 1, acc))

    // For exercise 3.17
    def dubToStr(l: List[Double]): List[String] =
      foldRightViaFoldLeft(l, Nil: List[String])((x, acc) => Cons(x.toString, acc))

    // For exercise 3.18
    def map[A, B](l: List[A])(f: A => B): List[B] =
      foldRightViaFoldLeft(l, Nil: List[B])((x, acc) => Cons(f(x), acc))

    // For exercise 3.19
    def filter[A](l: List[A])(f: A => Boolean): List[A] =
      foldRightViaFoldLeft(l, Nil: List[A])((x, acc) => if (f(x)) Cons(x, acc) else acc)

    // For exercise 3.20
    // We already wrote a function to "flatten" a list of lists, didn't we? Oh yeah, but we named
    // it 'concat' because the book said to concatenate two lists. So that just makes this a call
    // to 'map', followed by a call to 'concat'.
    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = concat(map(l)(f))

    // For exercise 3.21
    def filterViaFlatMap[A](l: List[A])(f: A => Boolean): List[A] =
      flatMap(l)(x => if (f(x)) List(x) else Nil: List[A])

    // For exercise 3.22
    // The solution in the official answers looks a lot cleaner than this, but isn't tail-recursive
    // or stack-safe. They start with "(a1, a2) match {..." and break out the head and tail sections
    // via a single match/case statement, rather than the nested form here. Their comments do note
    // that nesting match/case statements is slightly more efficient.
    // The behavior of this function was modelled after the behavior of the 'zip' function on the
    // standard List class, as observed in the REPL.
    def addElements(a1: List[Int], a2: List[Int]): List[Int] = {
      @tailrec
      def loopR(l1: List[Int], l2: List[Int], acc: List[Int]): List[Int] =
        l1 match {
          case Nil => acc
          case Cons(h, t) =>
            l2 match {
              case Nil => acc
              case Cons(h2, t2) => loopR(t, t2, Cons(h + h2, acc))
            }
        }
      // Because we "push" results into the accumulator List, we need to reverse the result list
      // before we return it to our caller.
      reverse(loopR(a1, a2, Nil: List[Int]))
    }

    // For exercise 3.23
    def zipWith[A, B, C](a1: List[A], a2: List[B])(f: (A, B) => C): List[C] = {
      def loopR(l1: List[A], l2: List[B], acc: List[C]): List[C] = {
        (l1, l2) match {
          case (Nil, _) => acc
          case (_, Nil) => acc
          case (Cons(h1, t1), Cons(h2, t2)) => loopR(t1, t2, Cons(f(h1, h2), acc))
        }
      }
      reverse(loopR(a1, a2, Nil))
    }

    // For exercise 3.24 (hard)
    def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = {
      def loopR(l1: List[A], l2: List[A]): Boolean =
        (l1, l2) match {
          case (Nil, Nil) => true
          case (Nil, _) => false
          case (Cons(_, _), Nil) => true
          case (Cons(h1, t1), Cons(h2, t2)) if h1 == h2 => (loopR(t1, t2) || loopR(t1, sub))
          case (Cons(_, t1), _) => loopR(t1, sub)
        }

      loopR(sup, sub)
    }
  }

  sealed trait Tree[+A]
  case class Leaf[A](value: A) extends Tree[A]
  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  object Tree {

    // For exercise 3.25
    def size[A](t: Tree[A]): Int =
      // Could not think of a tail recursive solution for this. Not sure it is possible.
      t match {
        case Leaf(_) => 1
        case Branch(l, r) => 1 + size(l) + size(r)
      }

    // For exercise 3.26
    def maximum(t: Tree[Int]): Int =
      t match {
        case Leaf(a) => a
        case Branch(l, r) => maximum(l) max maximum(r)
      }

    // For exercise 3.27
    def depth[A](t: Tree[A]): Int =
      t match {
        case Leaf(_) => 1
        case Branch(l, r) => (depth(l) max depth(r)) + 1
      }

    // For exercise 3.28
    def map[A, B](t: Tree[A])(f: A => B): Tree[B] =
      t match {
        case Leaf(a) => Leaf(f(a))
        case Branch(l, r) => Branch(map(l)(f), map(r)(f))
      }

    // For exercise 3.29
    def fold[A, B](t: Tree[A])(l: A => B)(b: (B, B) => B): B =
      t match {
        case Leaf(a) => l(a)
        case Branch(left, right) => b(fold(left)(l)(b), fold(right)(l)(b))
      }

    // For exercise 3.29
    def size2[A](t: Tree[A]): Int = fold(t)(_ => 1)((x, y) => 1 + x + y)

    // For exercise 3.29
    def maximum2(t: Tree[Int]): Int = fold(t)(identity)((x, y) => x max y)

    // For exercise 3.29
    def depth2[A](t: Tree[A]): Int = fold(t)(_ => 1)((x, y) => (x max y) + 1)

    // For exercise 3.29
    def map2[A, B](t: Tree[A])(f: A => B): Tree[B] = fold[A, Tree[B]](t)(leafVal => Leaf(f(leafVal)))((x, y) => Branch(x, y))

  }
}
