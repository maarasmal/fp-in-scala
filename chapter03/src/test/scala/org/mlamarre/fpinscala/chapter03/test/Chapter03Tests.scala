package org.mlamarre.fpinscala.chapter03.test

import org.mlamarre.fpinscala.chapter03.Chapter03._
import org.mlamarre.fpinscala.chapter03.Chapter03.List._
import org.mlamarre.fpinscala.chapter03.Chapter03.Tree._
import org.scalatest.{ FunSuite, Matchers }

/**
 * Contains tests that verify the solutions to the exercises in Chapter 3 of
 * Functional Programming in Scala.
 */
class Chapter03Tests extends FunSuite with Matchers {
  test("Exercise 3.1") {
    // This exercise was a bit weird in that it didn't ask you to write any
    // code, but to figure out what the result of the following match
    // expression would be:
    val x = List(1, 2, 3, 4, 5) match {
      case Cons(x, Cons(2, Cons(4, _))) => x
      case Nil => 42
      case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
      case Cons(h, t) => h + sum(t)
      case _ => 101
    }
    // My answer is: it should select the 3rd case, setting x to 3
    x should be(3)
  }

  test("Exercise 3.2") {
    tail(List(1, 2, 3)) should be(Cons(2, Cons(3, Nil)))
    an[UnsupportedOperationException] should be thrownBy tail(Nil)
  }

  test("Exercise 3.3") {
    setHead(1, List(5, 4, 3)) should be(Cons(1, Cons(4, Cons(3, Nil))))
    an[UnsupportedOperationException] should be thrownBy setHead(1, List())
  }

  test("Exercise 3.4") {
    drop(List(1, 2, 3, 4), 2) should be(Cons(3, Cons(4, Nil)))
    drop(Nil, 3) should be(Nil)
  }

  test("Exercise 3.5") {
    val l = List(1, 2, 3, 4, 5, 6)
    dropWhile(l, { x: Int => x < 4 }) should be(Cons(4, Cons(5, Cons(6, Nil))))
    dropWhile(l, { x: Int => x > 0 }) should be(Nil)
    dropWhile(Nil, { x: Int => x < 0 })
    dropWhile(List(1, 2, 3), { x: Int => x == 3 }) should be(Cons(1, Cons(2, Cons(3, Nil))))
  }

  test("Exercise 3.6") {
    init(List(1, 2, 3)) should be(List(1, 2))
    init(List("tom", "dick", "harry")) should be(List("tom", "dick"))
    an[UnsupportedOperationException] should be thrownBy init(Nil)
  }

  test("Exercise 3.7") {
    // There is nothing to test for this exercise, as it merely posed a thought experiment.
    // See the code in Chapter03.scala for that discussion.
  }

  test("Exercise 3.8") {
    // This exercise asked us to look at the result of the following call:
    val x = foldRight(List(1, 2, 3), Nil: List[Int])(Cons(_, _))
    // The result is that 'x' matches the original list:
    x should be(List(1, 2, 3))
    // As for "what this says about the relationship between foldRight and the data
    // constructors of List", I haven't the foggiest idea. It is basically a restatement
    // of the idea that the text mentioned a bit earlier in the chapter about how
    // foldRight "replaces the construcotrs of the list, Nil and Cons, with z and f"
  }

  test("Exercise 3.9") {
    // I think 'length' is a 'keyword' in ScalaTest, so we have to prepend "List." on these
    // calls to avoid compilation errors.
    List.length(List(1, 2, 3)) should be(3)
    List.length(List("foo", "bar")) should be(2)
    List.length(Nil) should be(0)
  }

  test("Exercise 3.10") {
    foldLeft(List(4, 5, 6), 0)(_ + _) should be(15)
    foldLeft(List("foo", "bar"), "")(_ + _) should be("foobar")
    foldLeft(Nil: List[Int], "")(_ * _) should be("")
  }

  test("Exercise 3.11") {
    sum3(List(1, 4, 5)) should be(sum(List(1, 4, 5)))
    sum3(Nil) should be(sum(Nil))

    product3(List(2.0, 4.0)) should be(product(List(2.0, 4.0)))
    product3(Nil: List[Double]) should be(product(Nil: List[Double]))

    // Again, we have to add "List." to the calls to length().
    length2(List("hi", "hello", "greetings", "bienvenue")) should be(List.length(List("hi", "hello", "greetings", "bienvenue")))
    length2(Nil) should be(List.length(Nil))
  }

  test("Exercise 3.12") {
    reverse(List(7, 8, 9)) should be(List(9, 8, 7))
    reverse(Nil) should be(Nil)
  }

  test("Exercise 3.13") {
    foldLeft(List("foo", "bar", "bam"), "L")(_ + _) should be("Lfoobarbam")
    foldLeftViaFoldRight(List("foo", "bar", "bam"), "L")(_ + _) should be
    (foldLeft(List("foo", "bar", "bam"), "L")(_ + _))
    foldRight(List("foo", "bar", "bam"), "R")(_ + _) should be("foobarbamR")
    foldRightViaFoldLeft(List("foo", "bar", "bam"), "R")(_ + _) should be
    (foldRight(List("foo", "bar", "bam"), "R")(_ + _))
  }

  test("Exercise 3.14") {
    append2(List(1, 2, 3), List(7, 8, 9)) should be(List(1, 2, 3, 7, 8, 9))
    append2(List(1, 2, 3), Nil) should be(List(1, 2, 3))
    append2(Nil, List(4, 5, 6)) should be(List(4, 5, 6))
    append2(Nil, Nil) should be(Nil)
  }

  test("Exercise 3.15") {
    concat(List(List(1, 2, 3), List(7, 8, 9))) should be(List(1, 2, 3, 7, 8, 9))
    concat(List(List(4, 5, 6), Nil, List(7, 8, 9))) should be(List(4, 5, 6, 7, 8, 9))
    concat(List(Nil, List(1, 2), List(11, 22, 33))) should be(List(1, 2, 11, 22, 33))
  }

  test("Exercise 3.16") {
    plusOne(List(3, 6, 9)) should be(List(4, 7, 10))
    plusOne(List((1 to 1000).toList: _*)) should be(List((2 to 1001).toList: _*))
    plusOne(Nil) should be(Nil)
  }

  test("Exercise 3.17") {
    dubToStr(List(1.0, 3.0)) should be(List("1.0", "3.0"))
    dubToStr(Nil: List[Double]) should be(Nil)
  }

  test("Exercise 3.18") {
    // I had to add "List." after I added the map() method to Tree.
    List.map(List(4, 5, 6))(_ + 1) should be(List(5, 6, 7))
    List.map(Nil: List[String])(_.toUpperCase) should be(Nil)
    List.map(List(1.0, 3.0))(_.toString) should be(List("1.0", "3.0"))
  }

  test("Exercise 3.19") {
    filter(List(1, 5, 10))(_ < 5) should be(List(1))
    filter(Nil: List[Int])(_ % 2 == 0) should be(Nil)
    filter(List("hi", "hello", "greetings"))(_.startsWith("h")) should be(List("hi", "hello"))
  }

  test("Exercise 3.20") {
    flatMap(List(1, 2, 3))(i => List(i, i)) should be(List(1, 1, 2, 2, 3, 3))
    flatMap(List(4, 6, 8))(i => List(i, i + 1)) should be(List(4, 5, 6, 7, 8, 9))
  }

  test("Exercise 3.21") {
    filterViaFlatMap(List(1, 5, 10))(_ < 5) should be(List(1))
    filterViaFlatMap(Nil: List[Int])(_ % 2 == 0) should be(Nil)
    filterViaFlatMap(List("hi", "hello", "greetings"))(_.startsWith("h")) should be(List("hi", "hello"))
  }

  test("Exercise 3.22") {
    addElements(List(1, 2, 3), List(1, 2, 3)) should be(List(2, 4, 6))
    addElements(List(1, 2, 3), List(10, 20, 30)) should be(List(11, 22, 33))
    addElements(Nil, List(1, 2, 3)) should be(Nil)
    addElements(List(2, 4, 6), Nil) should be(Nil)
    addElements(List(1, 2, 3), List(9)) should be(List(10))
    addElements(List(2), List(1, 2, 3)) should be(List(3))
  }

  test("Exercise 3.23") {
    zipWith(List(1, 2, 3), List(1, 2, 3))(_ + _) should be(List(2, 4, 6))
    zipWith(List(1, 2, 3), List(1, 2))(_ + _) should be(List(2, 4))
    zipWith(List(9, 8), List(1, 2, 3))(_ + _) should be(List(10, 10))
    zipWith(List(4, 5, 6), List(1, 2, 3))((x: Int, y: Int) => (x + y).toString) should be(List("5", "7", "9"))
    zipWith(List(4, 5, 6), List(1, 2, 3))(_ + _.toString) should be(List("41", "52", "63"))
  }

  test("Exercise 3.24") {
    hasSubsequence(List(1, 2, 3, 4), List(1, 2)) should be(true)
    hasSubsequence(List(1, 2, 3, 4), List(2, 3)) should be(true)
    hasSubsequence(List(1, 2, 3, 4), List(4)) should be(true)
    hasSubsequence(List(1, 2, 3, 4), List(1)) should be(true)
    hasSubsequence(List(1, 2, 3, 4), List(1, 2, 3, 4)) should be(true)
    hasSubsequence(List(1, 2, 3, 4), List(1, 2, 3, 4, 5)) should be(false)
    hasSubsequence(List(1, 2, 3, 4), List(1, 3)) should be(false)
    hasSubsequence(List(1, 2, 3, 4), List(4, 5)) should be(false)
  }

  // Test values for the Tree-related tests.
  val t1 = Leaf(2)
  val t2 = Branch(Leaf(1), Leaf(2))
  val t3 = Branch(Leaf(1), Branch(Leaf(2), Leaf(3)))
  // Same contents as the previous branch, but rebalanced.
  val t4 = Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))
  val t5 = Branch(Branch(Leaf(1), Leaf(2)), Branch(Branch(Leaf(3), Leaf(4)), Leaf(99)))
  val t6 = Branch(Branch(Leaf(45), Leaf(29)), Branch(Branch(Leaf(16), Leaf(62)), Branch(Leaf(1), Leaf(22))))

  test("Exercise 3.25") {
    Tree.size(t1) should be(1)
    Tree.size(t2) should be(3)
    Tree.size(t3) should be(5)
    Tree.size(t4) should be(5)
    Tree.size(t5) should be(9)
    Tree.size(t6) should be(11)
  }

  test("Exercise 3.26") {
    maximum(t1) should be(2)
    maximum(t3) should be(3)
    maximum(t5) should be(99)
    maximum(t6) should be(62)
  }

  test("Exercise 3.27") {
    depth(t1) should be(1)
    depth(t2) should be(2)
    depth(t3) should be(3)
    depth(t4) should be(3)
    depth(t5) should be(4)
    depth(t6) should be(4)
  }

  test("Exercise 3.28") {
    Tree.map(t1)(_ + 1) should be(Leaf(3))
    Tree.map(t2)(_ * 2) should be(Branch(Leaf(2), Leaf(4)))
    Tree.map(t5) { x: Int => (x + 1).toString } should be(Branch(Branch(Leaf("2"), Leaf("3")), Branch(Branch(Leaf("4"), Leaf("5")), Leaf("100"))))
  }

  test("Exercise 3.29") {
    Tree.size2(t1) should be(1)
    Tree.size2(t2) should be(3)
    Tree.size2(t3) should be(5)
    Tree.size2(t4) should be(5)
    Tree.size2(t5) should be(9)
    Tree.size2(t6) should be(11)

    maximum2(t1) should be(2)
    maximum2(t3) should be(3)
    maximum2(t5) should be(99)
    maximum2(t6) should be(62)

    depth2(t1) should be(1)
    depth2(t2) should be(2)
    depth2(t3) should be(3)
    depth2(t4) should be(3)
    depth2(t5) should be(4)
    depth2(t6) should be(4)

    Tree.map2(t1)(_ + 1) should be(Leaf(3))
    Tree.map2(t2)(_ * 2) should be(Branch(Leaf(2), Leaf(4)))
    Tree.map2(t5) { x: Int => (x + 1).toString } should be(Branch(Branch(Leaf("2"), Leaf("3")), Branch(Branch(Leaf("4"), Leaf("5")), Leaf("100"))))

  }

}
