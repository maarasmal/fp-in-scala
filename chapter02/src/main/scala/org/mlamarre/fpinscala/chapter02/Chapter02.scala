package org.mlamarre.fpinscala.chapter02

import scala.annotation.tailrec

object Chapter02 {
  def main(args: Array[String]) = println(s"Hello, world! I am Chapter02")

  /**
   * Calculates the Nth item in the Fibonnaci sequence (zero-based).
   * Exercise 2.1
   * @param n the index of the sequence to be calculated
   * @return the Nth item in the sequence
   */
  def fib(n: Int): Int = {
    @tailrec def loopR(x: Int, prev1: Int, prev2: Int): Int = {
      if (x == n) prev1 + prev2
      else loopR(x + 1, prev2, prev1 + prev2)
    }
    n match {
      case 0 => 0
      case 1 => 1
      case _ => loopR(2, 0, 1)
    }

  }

  /**
   * Determines whether an array is sorted according to the specified ordering function.
   * Exercise 2.2
   * @param as the array to be checked
   * @param ordered function defining the ordering of two items in the array
   * @tparam A the type of values in the array
   * @return true if the array elements are sorted according to 'ordered', otherwise false
   */
  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    @tailrec def loopR(arr: List[A]): Boolean = {
      arr match {
        case Nil => true
        case h :: Nil => true
        case a :: t => ordered(a, t.head) && loopR(t)
      }
    }

    loopR(as.toList)
  }

  // Exercise 2.3
  def curry[A, B, C](f: (A, B) => C): A => (B => C) = (a: A) => f(a, _)

  // Exercise 2.4
  def uncurry[A, B, C](f: A => B => C): (A, B) => C = (a: A, b: B) => f(a)(b)

  // Exercise 2.5
  def compose[A, B, C](f: B => C, g: A => B): A => C = (a: A) => f(g(a))

}

