lazy val commonSettings = Seq(
  organization := "com.mlamarre",
  version := "1.0.0-SNAPSHOT",
  scalaVersion := "2.12.1",
  libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)

lazy val root = project.in(file(".")).settings(commonSettings).settings(
  name := "mlamarre-fp-parent"
).aggregate(
  chapter01,
  chapter02,
  chapter03,
  chapter04,
  chapter05
)

lazy val chapter01 = project.settings(commonSettings).settings(
  name := "fp-chapter01"
)

lazy val chapter02 = project.settings(commonSettings).settings(
  name := "fp-chapter02"
).dependsOn(chapter01 % "compile->compile;test->test")

lazy val chapter03 = project.settings(commonSettings).settings(
  name := "fp-chapter03"
).dependsOn(chapter02 % "compile->compile;test->test")

lazy val chapter04 = project.settings(commonSettings).settings(
  name := "fp-chapter04"
).dependsOn(chapter03 % "compile->compile;test->test")

lazy val chapter05 = project.settings(commonSettings).settings(
  name := "fp-chapter05"
).dependsOn(chapter04 % "compile->compile;test->test")
