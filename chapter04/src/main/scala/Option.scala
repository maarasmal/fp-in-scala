
package org.mlamarre.fpinscala.chapter04

//hide std library `Option` and `Either`, since we are writing our own in this chapter
import scala.{ Option => _, Either => _, _ }
import scala.math

sealed trait Option[+A] {
  def foreach[U](f: A => U): Unit = {
    this match {
      case None => ()
      case Some(a) => f(a)
    }
  }
  def map[B](f: A => B): Option[B] =
    this match {
      case None => None
      case Some(a) => Some(f(a))
    }
  def flatMap[B](f: A => Option[B]): Option[B] = this.map(f).getOrElse(None)
  def getOrElse[B >: A](default: => B): B =
    this match {
      case None => default
      case Some(b) => b
    }
  def orElse[B >: A](ob: => Option[B]): Option[B] = this.map(Some(_)).getOrElse(ob)
  def filter(f: A => Boolean): Option[A] = this.flatMap { a => if (f(a)) Some(a) else None }

  def lift[A, B](f: A => B): Option[A] => Option[B] = _ map f

}

case object None extends Option[Nothing]
case class Some[+A](get: A) extends Option[A]

object Option {

  def apply[A](a: A): Option[A] = if (a == null) None else Some(a)

  def Try[A](a: => A): Option[A] =
    try Some(a)
    catch { case e: Exception => None }

  // For exercise 4.2
  def mean(xs: Seq[Double]): Option[Double] =
    xs match {
      case Seq() => None
      case _ => Some(xs.sum / xs.length)
    }

  def variance(ds: Seq[Double]): Option[Double] = {
    mean(ds).flatMap { m =>
      mean(ds.map(x => math.pow(x - m, 2)))
    }
  }

  def map2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] =
    (a, b) match {
      case (None, None) => None
      case (None, _) => None
      case (_, None) => None
      case (Some(x), Some(y)) => Some(f(x, y))
    }

  // The above implementation works, but (of course) the answer key has a
  // more elegant way of skipping Nones and extracting the inner values:
  // a flatMap (aa => b map (bb => f(aa, bb)))

  def sequence[A](as: List[Option[A]]): Option[List[A]] = {
    as.foldRight[Option[List[A]]](Some(List()))((oa, ol) => ol flatMap { l => oa.map(a => a :: l) })
    // This could also be implemented recursively, or the body of this foldRight could be implemented
    // using map2().
  }

  def traverse[A, B](as: List[A])(f: A => Option[B]): Option[List[B]] = {
    as.foldRight[Option[List[B]]](Some(Nil))((a, ol) => ol flatMap { l => f(a).map(_ :: l) })
  }

  def seq2[A](as: List[Option[A]]): Option[List[A]] = traverse(as)(identity)

}

