package org.mlamarre.fpinscala.chapter04.test

import org.mlamarre.fpinscala.chapter04._
import org.mlamarre.fpinscala.chapter04.Option.{ Try, variance, map2 }
import org.scalatest.{ FunSuite, Matchers }

/**
 * Contains tests that verify the solutions to the exercises in Chapter 3 of
 * Functional Programming in Scala.
 */
class Chapter04Tests extends FunSuite with Matchers {
  test("Exercise 4.1 - basic Option operations") {
    val n: Option[Int] = None
    val s: Option[Int] = Option(23)

    n should be(None)
    s should be(Some(23))

    n map (_ * 2) should be(None)
    s map (_ * 2) should be(Some(46))

    n flatMap (a => Some(a * 2)) should be(None)
    s flatMap (a => Some(a + 1)) shouldBe Some(24)
    s flatMap (a => None) shouldBe None

    n getOrElse 7 shouldBe 7
    s getOrElse 7 shouldBe 23

    n orElse Some(2) should be(Some(2))
    s orElse Some(9) should be(Some(23))

    n filter (_ % 2 == 0) should be(None)
    n filter (a => (a / a) == 1) should be(None)
    s filter (_ + 1 == 24) should be(Some(23))
  }

  test("Exercise 4.2 - variance using Option") {
    // Based on http://www.icoachmath.com/math_dictionary/variance.html
    val xs = Seq(1.0, 2.0, 3.0, 4.0, 10.0)
    val expectedVariance = 10.0
    variance(xs).foreach(_ should be(expectedVariance))

    // Expectations based on calculations obtained via https://www.easycalculation.com/statistics/standard-deviation.php
    val ys = Seq(13.0, 23.0, 12.0, 44.0, 55.0)
    val expecctedYVar = 296.24
    variance(ys).foreach(_ should be(expecctedYVar))

    variance(Seq[Double]()) should be(None)

    variance(Seq(0.0, 0.0, 0.0)) should be(Some(0.0))
  }

  test("Exercise 4.3 - map2 for Option") {
    map2(Some(1), Some(2))(_ + _) should be(Some(3))
    map2[Int, Int, Int](None, Some(2))(_ + _) should be(None)
    map2[Int, Int, Int](Some(1), None)(_ + _) should be(None)
    map2[Int, Int, Int](None, None)(_ + _) should be(None)
    map2(Some("abc"), Some("def"))((a, b) => a.toUpperCase + b.toUpperCase) should be(Some("ABCDEF"))
  }

  test("Exercise 4.4 - sequence for Option") {
    Option.sequence(List(Some(1), Some(2), Some(3))) should be(Option(List(1, 2, 3)))
    Option.sequence(List(Some(1), None, Some(2))) should be(None)
    Option.sequence(List[Option[Int]]()) should be(Option(List[Int]()))
  }

  test("Exercise 4.5 - traverse for Option") {
    Option.traverse(List(1, 2, 3))(a => Some(a + 1)) should be(Some(List(2, 3, 4)))
    Option.traverse(List("1", "2", "3"))(a => Try(a.toInt)) should be(Some(List(1, 2, 3)))
    Option.traverse(List("1", "2", "a"))(a => Try(a.toInt)) should be(None)

    // We reimplemented sequence in terms of traverse, so try that out a bit too
    Option.seq2(List(Some(1), Some(2), Some(3))) should be(Option.sequence(List(Some(1), Some(2), Some(3))))
    Option.seq2(List(Some(1), None, Some(2))) should be(Option.sequence(List(Some(1), None, Some(2))))
    Option.seq2(List[Option[Int]]()) should be(Option.sequence(List[Option[Int]]()))
  }

  test("Exercise 4.6 - basic operations on Either") {
    val left: Either[String, Int] = Left("error")
    val rite: Either[String, Int] = Right(23)

    left map (_ * 2) should be(left)
    rite map (_ * 2) should be(Right(46))

    left flatMap (a => Right(a * 2)) should be(left)
    rite flatMap (a => Right(a + 1)) shouldBe Right(24)
    rite flatMap (a => Left("oops")) shouldBe Left("oops")

    left getOrElse 7 shouldBe 7
    rite getOrElse 7 shouldBe 23

    left orElse Right(2) should be(Right(2))
    rite orElse Right(9) should be(Right(23))
  }

  test("Exercise 4.7 - sequence and traverse for Either") {
    val allSuccess = List(Right(23), Right(1), Right(3))
    Either.sequence(allSuccess) should be(Right(List(23, 1, 3)))
    Either.traverse(List(1, 2, 3))(x => Right(x + 1)) should be(Right(List(2, 3, 4)))

    val multFailures = List(Right(1), Left("err1"), Right(2), Left("err2"), Right(3))
    Either.sequence(multFailures) should be(Left("err1"))
    Either.traverse(List(1, 2, 3, 4)) { x => if (x % 2 == 0) Left("oops" + x) else Right(x + 1) } should be(Left("oops2"))
  }

  test("Exercise 4.8 - something like Scalaz's Validation") {
    pending
  }

}
