package org.mlamarre.fpinscala.chapter02.test

import org.mlamarre.fpinscala.chapter02.Chapter02._
import org.scalatest.{ FunSuite, Matchers }

class Chapter02Tests extends FunSuite with Matchers {

  test("Exercise 2.1") {
    fib(0) should be(0)
    fib(1) should be(1)
    fib(2) should be(1)
    fib(3) should be(2)
    fib(4) should be(3)
    fib(5) should be(5)
    fib(6) should be(8)
    fib(7) should be(13)
    fib(8) should be(21)
    fib(9) should be(34)
    fib(10) should be(55)
  }

  test("Exercise 2.2") {
    isSorted[Int](Array(1, 2, 3, 4), (x, y) => x < y) should be(true)
    isSorted[Int](Array(1, 2, 3, 4, 2), (x, y) => x < y) should be(false)
    isSorted[Int](Array(4, 3, 2, 1), (x, y) => x < y) should be(false)
    isSorted[Int](Array(4, 3, 2, 1), (x, y) => x > y) should be(true)
    isSorted[String](Array("apple", "boy", "charlie"), (s1, s2) => s1 < s2) should be(true)
    isSorted[String](Array("mike", "esmond", "geoffrey"), (s1, s2) => s1 < s2) should be(false)
  }

  val mysum = (x: Int, y: Int) => x + y

  test("Exercise 2.3") {
    val curriedSum = curry(mysum)
    val sum3 = curriedSum(3)
    sum3(3) should be(6)
    sum3(1) should be(4)
    sum3(99) should be(102)

    // The stuff above shows partially applied functions, but really, the value or "meaning" behind
    // currying is this equality here.
    curriedSum(3)(4) should be(7)
    curriedSum(2)(7) shouldEqual mysum(2, 7)

    val strIntCat = (s: String, x: Int) => s + x.toString
    val curriedCat = curry(strIntCat)
    val hello = curriedCat("Hello")
    hello(12) should be("Hello12")
    hello(99) should be("Hello99")
  }

  test("Exercise 2.4") {
    val curriedSum = curry(mysum)
    val uncurriedSum = uncurry(curriedSum)
    uncurriedSum(1, 2) shouldEqual curriedSum(1)(2)
  }

  test("Exercise 2.5") {
    val plus1 = (x: Int) => x + 1
    val times2 = (y: Int) => y * 2

    val comp = compose(plus1, times2)

    comp(2) should be(5)
    comp(5) should be(11)

    compose(times2, plus1)(1) should be(4)
    compose(times2, plus1)(11) should be(24)
  }
}

